const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const getSlug = require('speakingurl');

const articleSchema = new Schema ({
    title : {
        type: String,
        unique: true,
        required: true,
    },
    description : {
        type: String,
        required: true,
    },
    content : {
        type: String,
        required: true,
    },
    author : {
        type : Schema.Types.ObjectId,
        ref : 'users',
        // type : String,
    },
    url : {
        type: String,
        trim : true
    }
},{
    timestamps:true,
}
); 

articleSchema.pre('save', function (next) {
    const article = this;
    if (!article.isModified('title')) return next();
    const url = getSlug(article.title, {
        lang: 'vn',
    });
    article.url = url;
    console.log(url);
    next();
});


module.exports = mongoose.model('article', articleSchema);