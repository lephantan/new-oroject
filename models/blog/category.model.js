const mongoose = require('mongoose');
const getSlug = require('speakingurl');
const Schema = mongoose.Schema;



const categorySchema = new Schema ({
    name : {
        type: String,
        unique: true,
        required: true,
        trim : true,
    },
    url : {
        type: String,
        unique: true,
        trim : true,
    },
},{
    timestamps:true,
}
); 

categorySchema.pre('save', function (next) {
    const category = this;
    if (!category.isModified('title')) return next();
    const url = getSlug(category.title, {
        lang: 'vn',
    });
    category.url = url;
    
    next();
});


module.exports = mongoose.model('categories', categorySchema);