const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;



const userSchema = new Schema({
    username:{
        type: String,
        unique: true,
        required:true,
    },
    email: {
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    avatar: {
        type : String,
        default: 'http://farm9.staticflickr.com/8130/29541772703_6ed8b50c47_b.jpg',

    }
},  {
    timestamps: true,
});

userSchema.pre('save',function(next) {
    const user = this;

    if(!user.isModified('password')) return next();

    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(user.password, salt, (err,hash) => {
            if (err) return next(err);

            user.password = hash;
            next();
        })
    })
})

module.exports = mongoose.model('users',userSchema);