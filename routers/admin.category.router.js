const express = require('express');
const router = express.Router();

const {
    addCategory,
    getAllCategories,
} = require('../controllers/admin.category.controller');

const {checkCategory} =require ('../config/validate');

router.route('/add')
    .get((req, res) => {
        res.render('admin/category_add');
    })
    .post(checkCategory,(req, res, next) => {
        addCategory(req, res, next);
    });


router.get('/manage', (req, res, next) => {
    getAllCategories(req, res, next);
});

module.exports = router;