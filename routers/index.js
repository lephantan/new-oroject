const express = require('express');

const router = express.Router();

const routerAdmin = require('./admin.router');

const routerClient = require('./client.router');

router.use('/', routerClient);

router.use('/admin', routerAdmin);

module.exports = router;