const express = require('express');
const router = express.Router();

const {
    checkPost
} = require('../config/validate');

const {
    middlewareModify,
    addArticle,
    manageArticle,
    deleteArticle,
    getArticle,
    editArticle,
    getArticlesByAuthor
} = require('../controllers/admin.article.controller');

router.use(['/delete/:id','/edit/:id'],(req,res,next)=>{
    middlewareModify(req,res,next);
})

router.get('/add', (req, res) =>{
    res.render('admin/add'); 
});

router.post('/add',checkPost , (req, res) => {
    addArticle(req,res);
});

router.get('/delete/:id',(req,res)=>{
    deleteArticle(req,res);
});

router.route('/edit/:id')
    .get((req,res)=> {
        getArticle(req,res);
    })
    .post(checkPost,(req,res) => {
        editArticle(req,res);
    })

router.get('/manage' , (req,res) => {
    manageArticle(req,res);
})

router.get('/manage-by-author',(req,res)=> {
    getArticlesByAuthor(req,res);
})

module.exports = router;