const express = require('express');
const { check, validationResult } = require('express-validator/check');
const User = require('../models/users/user.model');
const router = express.Router();
const bcrypt = require('bcryptjs');

const {
    loginController,
    registerController,
    logoutController
} = require('../controllers/admin.auth.controller');



router.use((req, res, next) => {
    res.locals.flash_message = req.session.flash;
    res.locals.cUser = req.session.Users;
    delete req.session.flash;
    next();
});

router.post('/login',(req,res) => {
    loginController(req,res);
});

router.get('/login',(req,res) =>{
    if(req.session.Users) res.redirect('/admin');
    else res.render('admin/login');
});

router.post('/register', (req,res)=>{
    registerController(req,res);
});

router.use((req,res,next) => {
    if(!req.session.Users) return res.redirect('/admin/login');

    next();
});

router.get('/logout',(req,res) =>{
    logoutController(req,res);
});

router.get('/' , (req, res) =>{
    res.render('admin/index');
});








module.exports = router;