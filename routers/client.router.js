const express = require('express');
const router = express.Router();

const {
    getAllArticles,
    getArtticleByUrl
 } = require('../controllers/client.article.controller');

router.get('/' , (req, res, next) =>{
    getAllArticles(req,res,next);
});

router.get('/about', (req, res) =>{
    res.render('client/about', {
        img :'about',
    }); 
});

router.get('/post/:url', (req,res,next) => {
    getArtticleByUrl(req,res,next)
})

router.get('/contact', (req, res) => {
    res.render('client/contact', {
        img: 'contact',
    });
});
module.exports = router;