const {
    check,
    validationResult
} = require('express-validator/check');
const Article = require('../models/blog/article.model');
const Category = require('../models/blog/category.model');

const middlewareModify = (req,res,next) => {
    const articleId = req.params.id;
    const userId = req.session.Users.id;
    Article
        .findById(articleId)
        .exec((err, article) => {
            if (err) {
                console.log(err);
                return next(err);
            };
            if(!article) return next (new Error('Article not found'));
            if(article.author.toString() !== userId) {
                req.flash('danger','Access denied')
                return res.redirect('/admin/article/manage');
            };
            next();
        }); 
};


const addArticle = (req, res) => {
    const {
        title,
        description,
        content,
        author,
    } = req.body;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        errors.array().forEach(e => {
            req.flash('danger', e.msg);

        });
        req.flash('objBody', {
            title,
            description,
            content,
        })

        return res.redirect('/admin/article/add');
        // return res.render('admin/add', {
        //     title, description , content,author
        // })
    }

    const article = new Article({
        title,
        description,
        content,
        author,
    });

    article.save((err) => {
        if (err) {
            console.log(err);
            return;
        }
        res.redirect('/admin/article/manage')
    });
};

const getArticlesByAuthor = (req,res) =>{
    const authorid = req.session.Users.id;

    Article
        .find({
            author : authorid
        })
        .populate('author', '-_id username')
        .sort({
            updatedAt: -1,
        })
        .select('_id title description content createdAt author url')
        .exec((err, articles) => {
            if (err) {
                console.log(err);
                return;
            }
            res.render('admin/manage-by-author', {
                articles
            })
        })
};

const manageArticle = (req, res) => {
    Article
        .find()
        .populate('author', '-_id username')
        .sort({
            updatedAt: -1,
        })
        .select('_id title description content createdAt author url updatedAt')
        .exec((err, articles) => {
            if (err) {
                console.log(err);
                return;
            }
            res.render('admin/manage', {
                articles
            })
        })
};

const deleteArticle = (req, res) => {
    Article.deleteOne({
        _id: req.params.id
    }, (err) => {
        if (err) {
            console.log(err);
            req.flash('danger', 'Delete article failed');
            return res.redirect('/admin/article/add');
        }
        req.flash('success','Delete Article successfully');
        res.redirect('/admin/article/manage');
    })
};

const getArticle = (req,res) =>{
    Article
        .findById(req.params.id)
        .exec((err,article)=>{
            if (err) {
                console.log(err);
                return;
            };
            if (!article){
                req.flash('danger','Can not found article');
                return res.redirect('/admin/article/manage');
            };
            res.render('admin/edit', {
                article
            });
        });
};

const editArticle = (req,res) =>{
    const {
        title,
        description,
        content,
        author,
    } = req.body;
    const {
        id
    }= req.params;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        errors.array().forEach(e => {
            req.flash('danger', e.msg);

        });
        req.flash('objBody', {
            title,
            description,
            content,
        })

        return res.redirect(`/admin/article/edit/${id}`);
    }

    Article.findById({
        _id : id
    },(err,article)=>{
        if (err) {
            console.log(err);
            return;
        };
        if (!article){
            req.flash('danger','Can not found article');
            return res.redirect('/admin/article/manage');
        };
        article.title = title;
        article.description = description;
        article.content = content;

        article.save((err) => {
            if (err) {
                console.log(err);
                return;
            }
            res.redirect('/admin/article/manage')
        });
    })

    
}

module.exports = {
    middlewareModify,
    addArticle,
    getArticlesByAuthor,
    manageArticle,
    deleteArticle,
    getArticle,
    editArticle,
};