const Article = require('../models/blog/article.model');

const getAllArticles = (req,res,next) => {
    Article
        .find()
        .select('-_id -createdAt -content -__v')
        .populate('author','-_id username')
        .exec((err, articles)=>{
            if (err) {
                const cError = new Error('internal server error');
                cError.status = 500 ;
                return next(cError);
            }

            console.log(articles);

            res.render('client/index', {
                img : 'home',
                articles
            })
        });
}

const getArtticleByUrl = (req,res,next) => {

    const url = req.params.url;
    Article
        .findOne({ url })
        .select('-_id -createdAt -__v')
        .populate('author','-_id username')
        .exec((err, article)=>{
            if (err) {
                const cError = new Error('internal server error');
                cError.status = 500 ;
                return next(cError);
            }

            if (!article) {
                const cError = new Error('Not Found');
                cError.status = 404;
                return next(cError);
            }

            console.log(article);

            res.render('client/post', {
                img : 'post',
                article
            })
        });
}

module.exports = {
    getAllArticles,
    getArtticleByUrl,
}