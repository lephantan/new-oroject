const bcrypt = require('bcryptjs');
const User = require('../models/users/user.model');
const { check, validationResult } = require('express-validator/check');


const loginController = (req,res) => {
    const {
        username,
        password,
    }= req.body;

    User.findOne({
        username: username,
    }, (err,user) =>{
        if (err){
            console.log(err);
            return;
        }
        if (user) {
            if (bcrypt.compareSync(password, user.password)) {
                req.session.Users = {
                    id: user._id,
                    username : user.username,
                    avatar : user.avatar,
                    email: user.email,
                };
                
                
                req.flash('success','xin chào');
                res.redirect('/admin');
            } else {
            console.log('sai mat khau');
            req.flash('danger','tên đăng nhập hoặc mật khẩu không đúng');
            res.redirect('/admin/login');
            }
        } else {
            console.log('khong tim thay username');
            res.redirect('/admin/login');
        }
    })
}

const registerController = (req,res) => {
    const {
        username,
        email,
        password,
    } = req.body;
    const cUser = new User({
        username,
        email,
        password,
    });
   

    cUser.save((err,user) =>{
        if (err) {
            console.log('>>>error register',err);
            return;
        }

        console.log('>>>User:',user);
        res.redirect('/admin');
    })
}

const logoutController = (req,res) =>{
    req.session.destroy((err) =>{
        if (err) {
            console.log(err);
            return
        }
        res.redirect('/admin/login');
    });
}

module.exports = {
    loginController,
    registerController,
    logoutController,
};