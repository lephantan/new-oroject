const Category = require('../models/blog/category.model');

const {
    check,
    validationResult
} = require('express-validator/check');

const getAllCategories = (req, res, next) => {
    Category
        .find()
        .select('-createdAt -__v')
        .sort({name: 1})
        .exec((err, categories) => {
            if (err) {
                const error = new Error('Internal Server Error');
                error.status = 500;
                return next(err);
            };
            res.render('admin/category_manage', {
                categories
            });
        });
};

const addCategory = (req, res, next) => {
    const {
        name
    } = req.body;
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        errors.array().forEach(error =>{
            req.flash(error.param,error.msg);
        });
        res.redirect('/admin/category/add');
    }
    const category = new Category({
        name
    });
    category.save((err) => {
        if (err) {
            const error = new Error('Internal Server Error');
            error.status = 500;
            return next(err);
        };
        req.flash('success', 'Add category successful');
        res.redirect('/admin/category/manage');
    });
};

module.exports = {
    getAllCategories,
    addCategory,
};