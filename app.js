const express = require ('express');
const router = require('./routers');
const mongoose = require('mongoose');
const { port,db } = require ('./config/config');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const flash = require('connect-flash');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

mongoose.Promise = global.Promise;
mongoose.connect(db,{ useNewUrlParser: true });
mongoose.connection.on('error',(err) =>{
    console.log('>>> error database',err);
});

const app = express();
app.set('view engine','ejs');
app.set('views','./views');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/public', express.static('./public'));
app.use(morgan('dev'));
app.use(flash());
app.use(session({
    name: 'blog',
    secret : 'sessionSecret' ,
    resave : false,
    saveUninitialized: true,
    store : new MongoStore({ url : db }),
}));
app.use(router);

app.listen (port,(err) =>{
    if (err){
        console.log ('Lỗi server');
        return;
    }
    console.log ('Server running on port', port);
});