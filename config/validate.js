const {check, validationResult} = require('express-validator/check');

const checkPost = [
    check('title').trim().not().isEmpty().withMessage('Tiêu đề không được bỏ trống'),
    check('content').not().isEmpty().withMessage('Content không được để trống'),
];

const checkCategory = [
    check('name').trim().not().isEmpty().withMessage('Tên chuyên mục không được để trống'),
];

module.exports = {
    checkPost,
    checkCategory,
}